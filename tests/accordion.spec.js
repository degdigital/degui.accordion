import accordion from '../src/accordion';

console.error = jest.fn();

describe('accordion', () => {

    let containerEl;

    beforeEach(() => {
        document.body.innerHTML = `
            <div class="wrapper">
                <section id="section-1">
                    <h1>Section 1</h1>
                    <div>Section 1 content</div>
                </section>        
                <section id="section-2">
                    <h1>Section 2</h1>
                    <div>Section 2 content</div>
                </section>        
                <section id="section-3">
                    <h1>Section 3</h1>
                    <div>Section 3 content</div>
                </section>        
            </div>`;

        containerEl = document.querySelector('.wrapper');
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should console error if no container el given', () => {
        accordion();
        expect(console.error).toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledWith('Accordion Error: no container element provided to accordion.')
    });

    describe('initialization', () => {
        it('should initialize default HTML', () => {
            accordion(containerEl);

            expect(document.body.innerHTML).toMatchSnapshot();
        });

        it('should initialize custom HTML', () => {
            accordion(containerEl, {
                wrapperCssClass: 'accordion-custom',
                panelCssClass: 'accordion-custom__panel',
                triggerCssClass: 'accordion-custom__trigger',
                contentCssClass: 'accordion-custom__content',
                openClass: 'is-open-custom'
            });

            expect(document.body.innerHTML).toMatchSnapshot();
        });
    });

    describe('opening a panel', () => {
        it('should add open class to panel', () => {
            accordion(containerEl, {allowMultiplePanelsOpen: true, initialOpenPanelIndices: []}).openPanel(0);

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            expect(contentWrapperEls[0].classList.contains('is-open')).toBe(true);
        });

        it('should update aria-expanded attribute', () => {
            accordion(containerEl, {allowMultiplePanelsOpen: true, initialOpenPanelIndices: []}).openPanel(0);
      
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
            expect(triggerEls[0].getAttribute('aria-expanded')).toBe('true');
            expect(triggerEls[1].getAttribute('aria-expanded')).toBe('false');
            expect(triggerEls[2].getAttribute('aria-expanded')).toBe('false');
        });

        it('should update aria-hidden attribute', () => {
            accordion(containerEl, {allowMultiplePanelsOpen: true, initialOpenPanelIndices: []}).openPanel(0);
           
            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('false');
            expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('true');
            expect(contentWrapperEls[2].getAttribute('aria-hidden')).toBe('true');
        });

        it('should collapse other panels', () => {
            accordion(containerEl, {initialOpenPanelIndices: []}).openPanel(0);

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            expect(contentWrapperEls[1].classList.contains('is-open')).toBe(false);
            expect(contentWrapperEls[2].classList.contains('is-open')).toBe(false);
        });
    });

    describe('closing a panel', () => {
        const closeConfig = {
            allowMultiplePanelsOpen: true,
            initialOpenPanelIndices: [0]
        };
      
        it('should remove open class to panel', () => {
            accordion(containerEl, closeConfig).closePanel(0);

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            expect(contentWrapperEls[0].classList.contains('is-open')).toBe(false);
        });

        it('should update aria-expanded attribute', () => {
            accordion(containerEl, closeConfig).closePanel(0);

            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
            expect(triggerEls[0].getAttribute('aria-expanded')).toBe('false');
        });

        it('should update aria-hidden attribute', () => {
            accordion(containerEl, closeConfig).closePanel(0);

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('true');
        });
    });

    it('should open all', () => {      
        accordion(containerEl, {allowMultiplePanelsOpen: true}).openAllPanels();

        const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
        const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

        expect(contentWrapperEls[0].classList.contains('is-open')).toBe(true);
        expect(contentWrapperEls[1].classList.contains('is-open')).toBe(true);
        expect(contentWrapperEls[2].classList.contains('is-open')).toBe(true);

        expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('false');
        expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('false');
        expect(contentWrapperEls[2].getAttribute('aria-hidden')).toBe('false');

        expect(triggerEls[0].getAttribute('aria-expanded')).toBe('true');
        expect(triggerEls[1].getAttribute('aria-expanded')).toBe('true');
        expect(triggerEls[2].getAttribute('aria-expanded')).toBe('true');

    });

    it('should close all', () => {
        accordion(containerEl, {allowMultiplePanelsOpen: true}).closeAllPanels();
        
        const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
        const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

        expect(contentWrapperEls[0].classList.contains('is-open')).toBe(false);
        expect(contentWrapperEls[1].classList.contains('is-open')).toBe(false);
        expect(contentWrapperEls[2].classList.contains('is-open')).toBe(false);

        expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('true');
        expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('true');
        expect(contentWrapperEls[2].getAttribute('aria-hidden')).toBe('true');

        expect(triggerEls[0].getAttribute('aria-expanded')).toBe('false');
        expect(triggerEls[1].getAttribute('aria-expanded')).toBe('false');
        expect(triggerEls[2].getAttribute('aria-expanded')).toBe('false');
    });

    describe('initial opening', () => {
        afterEach(() => {
            window.location.hash = '';
        });

        it('should open panel with corresponding id in url hash', () => {
            window.location.hash = 'section-2';

            accordion(containerEl);

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            expect(contentWrapperEls[1].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('false');
            expect(triggerEls[1].getAttribute('aria-expanded')).toBe('true');
        });

        it('should open first panel by default', () => {
            accordion(containerEl);
            
            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            expect(contentWrapperEls[0].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('false');
            expect(triggerEls[0].getAttribute('aria-expanded')).toBe('true');
        });
    
        it('should open panel if initialized with initialOpenPanelIndices', () => {
            accordion(containerEl, {
                initialOpenPanelIndices: [1]
            });
            
            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            expect(contentWrapperEls[1].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('false');
            expect(triggerEls[1].getAttribute('aria-expanded')).toBe('true');
        });
    
        it('should open multiple panels if initialized with multiple initialOpenPanelIndices', () => {
            accordion(containerEl, {
                allowMultiplePanelsOpen: true,
                initialOpenPanelIndices: [0, 1]
            });

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            expect(contentWrapperEls[0].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[1].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('false');
            expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('false');
            expect(triggerEls[0].getAttribute('aria-expanded')).toBe('true');
            expect(triggerEls[1].getAttribute('aria-expanded')).toBe('true');
        });

        it('should give preference to hash', () => {
            window.location.hash = 'section-3';

            accordion(containerEl, {
                initialOpenPanelIndices: [1]
            });

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            expect(contentWrapperEls[1].classList.contains('is-open')).toBe(false);
            expect(contentWrapperEls[2].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('true');
            expect(contentWrapperEls[2].getAttribute('aria-hidden')).toBe('false');
            expect(triggerEls[1].getAttribute('aria-expanded')).toBe('false');
            expect(triggerEls[2].getAttribute('aria-expanded')).toBe('true');
        });

        it('should handle hash not correlating to panel', () => {
            window.location.hash = 'non-existent-section-id';

            accordion(containerEl, {
                initialOpenPanelIndices: [1]
            });

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            expect(contentWrapperEls[0].classList.contains('is-open')).toBe(false);
            expect(contentWrapperEls[1].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[2].classList.contains('is-open')).toBe(false);
            expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('true');
            expect(contentWrapperEls[1].getAttribute('aria-hidden')).toBe('false');
            expect(contentWrapperEls[2].getAttribute('aria-hidden')).toBe('true');
            expect(triggerEls[0].getAttribute('aria-expanded')).toBe('false');
            expect(triggerEls[1].getAttribute('aria-expanded')).toBe('true');
            expect(triggerEls[2].getAttribute('aria-expanded')).toBe('false');
        });
    });

    describe('trigger click', () => {       
        it('should open panel related to button clicked', () => {      
            accordion(containerEl, {
                allowMultiplePanelsOpen: true,
                initialOpenPanelIndices: []
            });

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            triggerEls[0].click();

            expect(contentWrapperEls[0].classList.contains('is-open')).toBe(true);
            expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('false');
            expect(triggerEls[0].getAttribute('aria-expanded')).toBe('true');
        });

        it('should close panel related to button clicked if open', () => {
            accordion(containerEl, {
                allowMultiplePanelsOpen: true,
                initialOpenPanelIndices: [0]
            });

            const contentWrapperEls = [...containerEl.querySelectorAll('.accordion__content')];
            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            triggerEls[0].click();

            expect(contentWrapperEls[0].classList.contains('is-open')).toBe(false);
            expect(contentWrapperEls[0].getAttribute('aria-hidden')).toBe('true');
            expect(triggerEls[0].getAttribute('aria-expanded')).toBe('false');
        })
    });

    describe('accessibility', () => {

        describe('pressing up arrow', () => {

            it('should focus previous accordion header', () => {
                accordion(containerEl, {allowMultiplePanelsOpen: true});

                const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
                triggerEls[1].focus();

                const keydownEvent = new KeyboardEvent('keydown', { key: 'ArrowUp'});
                triggerEls[1].dispatchEvent(keydownEvent);

                expect(document.activeElement).toBe(triggerEls[0]);
            });

            it('if first, should focus last accordion header', () => {
                accordion(containerEl, {allowMultiplePanelsOpen: true});

                const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
                triggerEls[0].focus();

                const keydownEvent = new KeyboardEvent('keydown', { key: 'ArrowUp'});
                triggerEls[0].dispatchEvent(keydownEvent);

                expect(document.activeElement).toBe(triggerEls[2]);
            });

            it('should handle IE up key value', () => {
                accordion(containerEl, {allowMultiplePanelsOpen: true});

                const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
                triggerEls[1].focus();

                const keydownEvent = new KeyboardEvent('keydown', { key: 'Up'});
                triggerEls[1].dispatchEvent(keydownEvent);

                expect(document.activeElement).toBe(triggerEls[0]);
            });
        });

        describe('pressing down arrow', () => {

            it('should focus next accordion header', () => {
                accordion(containerEl, {allowMultiplePanelsOpen: true});

                const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
                triggerEls[1].focus();

                const keydownEvent = new KeyboardEvent('keydown', { key: 'ArrowDown'});
                triggerEls[1].dispatchEvent(keydownEvent);

                expect(document.activeElement).toBe(triggerEls[2]);
            });

            it('if last, should focus first accordion header', () => {
                accordion(containerEl, {allowMultiplePanelsOpen: true});

                const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
                triggerEls[2].focus();

                const keydownEvent = new KeyboardEvent('keydown', { key: 'ArrowDown'});
                triggerEls[2].dispatchEvent(keydownEvent);

                expect(document.activeElement).toBe(triggerEls[0]);
            });

            it('should handle IE down key value', () => {
                accordion(containerEl, {allowMultiplePanelsOpen: true});

                const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
                triggerEls[1].focus();

                const keydownEvent = new KeyboardEvent('keydown', { key: 'Down'});
                triggerEls[1].dispatchEvent(keydownEvent);

                expect(document.activeElement).toBe(triggerEls[2]);
            });
        });

        it('should focus first header when pressing home', () => {
            accordion(containerEl, {allowMultiplePanelsOpen: true});

            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
            triggerEls[1].focus();

            const keydownEvent = new KeyboardEvent('keydown', { key: 'Home'});
            triggerEls[1].dispatchEvent(keydownEvent);

            expect(document.activeElement).toBe(triggerEls[0]);
        });

        it('should focus last header when pressing End', () => {
            accordion(containerEl, {allowMultiplePanelsOpen: true});

            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];
            triggerEls[1].focus();

            const keydownEvent = new KeyboardEvent('keydown', { key: 'End'});
            triggerEls[1].dispatchEvent(keydownEvent);

            expect(document.activeElement).toBe(triggerEls[2]);
        });
    });

    describe('events', () => {
        it('should fire a openPanel event when a panel opens', () => {
            const accordionInst = accordion(containerEl);
            
            const handler = jest.fn();
            accordionInst.addEventListener('degui.accordion:panelOpen', handler);
            accordionInst.openPanel(1);
            
            expect(handler).toHaveBeenCalledTimes(1);
            expect(handler).toHaveBeenCalledWith(expect.objectContaining({
                target: containerEl,
                detail: { panelIndex: 1 }
            }));
        });

        it('should fire a closePanel event when a panel closes', () => {
            const accordionInst = accordion(containerEl);
            
            const handler = jest.fn();
            accordionInst.addEventListener('degui.accordion:panelClose', handler);
            accordionInst.closePanel(0);
            
            expect(handler).toHaveBeenCalledTimes(1);
            expect(handler).toHaveBeenCalledWith(expect.objectContaining({
                target: containerEl,
                detail: { panelIndex: 0 }
            }));
        });

        it('should fire an event that bubbles', () => {
            const accordionInst = accordion(containerEl);
            
            const handler = jest.fn();
            document.addEventListener('degui.accordion:panelOpen', handler);
            accordionInst.openPanel(1);
            
            expect(handler).toHaveBeenCalledTimes(1);
            expect(handler).toHaveBeenCalledWith(expect.objectContaining({
                target: containerEl,
                detail: { panelIndex: 1 }
            }));
        });

        it('should remove an event listener', () => {
            const accordionInst = accordion(containerEl);
            
            const handler = jest.fn();
            accordionInst.addEventListener('degui.accordion:panelOpen', handler);
            accordionInst.removeEventListener('degui.accordion:panelOpen', handler);
            accordionInst.openPanel(1);
            
            expect(handler).toHaveBeenCalledTimes(0);
        });
    });

    describe('cleanup', () => {
        it('should remove all event listeners', () => {
            const accordionInst = accordion(containerEl);

            const triggerEls = [...containerEl.querySelectorAll('.accordion__trigger')];

            const removeEventListenerSpy = jest.spyOn(triggerEls[0], 'removeEventListener');
            
            accordionInst.destroy();

            expect(removeEventListenerSpy).toHaveBeenCalledTimes(2);
            expect(removeEventListenerSpy).toHaveBeenNthCalledWith(1, 'click', expect.any(Function));
            expect(removeEventListenerSpy).toHaveBeenNthCalledWith(2, 'keydown', expect.any(Function));
        });

        it('should restore original HTML', () => {
            const accordionInst = accordion(containerEl);
            accordionInst.openPanel(1);
            accordionInst.destroy();

            expect(document.body.innerHTML).toMatchSnapshot();

        });
    });

});