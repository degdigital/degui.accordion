# Accordion
The accordion plugin transforms a group of content sections into collapsible content panels. An accorrdion is useful for displaying information in a limited amount of space. It can also be a good alternative to a tabbed interface when horizontal space is restricted.

## Assumptions
- All sections are rendered before the accordion plugin is instantiated, and sections are not added/removed dynamically
- Each section element has a predefined, unique `id` attribute
- If there is a hash in the url that corresponds to a section element's `id`, that section element will automatically open on page load.

## HTML Structure
The accordion plugin transforms existing HTML. The structure of this HTML should be similar to the following:
```html
<div>
    <section id="section-1">
        <h1>Section 1 Title</h1>
        <div>Section 1 description</div>
    </section>
    <section id="section-2">
        <h1>Section 2 Title</h1>
        <div>Section 2 description</div>
    </section>
</div>
```
The specific HTML tags themselves can vary as needed. In general, there should be a container element with one or more section elements inside of it. Each section element should have a title element and a content element. These section elements will become accordion panels once the plugin is instantiated.

## Usage
The build scripts for this project use fs.copy that were introduced in node v8.

### CSS
The accordion component is shipped with one CSS file that handles the functional CSS of showing/hiding the accordion panels. 

Using postCSS and a postCSS import plugin, you can include the CSS in your project by including
```css
@import '@deg-she-ra/accordion/dist/*.css';
```
in one of your project CSS files.

### Javascript
To instantiate the accordion component, import it and pass the necessary parameters to the module.

Example:
```js
import sheRaAccordion from '@deg-she-ra/accordion';

const accordionInst = sheRaAccordion(containerElement, {
    // accordion options
})

accordionInst.openAllPanels();
```

#### Transpiling

If using the Babel Rollup plugin to transpile your JS, make sure to transpile this component by adding it to the `include` list in the plugin config.

For example,
```js
require('rollup-plugin-babel')({
    include: [
        'node_modules/@deg-she-ra/accordion/**'
    ]
})
```

or to include all She-Ra components in transpilation, configure the plugin like
```js
require('rollup-plugin-babel')({
    include: [
        'node_modules/@deg-she-ra/**'
    ]
}),
```

#### Options
Passed in options can be used to override defaults.

| Property Name | Description | Type | Default |
| ------------- | ----------- | ---- | ------- |
| allowMultiplePanelsOpen | Describes if multiple panels can be open at one time. | `Boolean` | `false` |
| initialOpenPanelIndices | A list of panel indices that should be open initially. If `allowMultiplePanelsOpen` is false, only the first index in the list will be opened. | `Number[]` | `[0]` |
| panelSelector | The DOM selector for each section element to be transformed into an accordion panel. | `String` | `'section'` |
| titleSelector | The DOM selector for each panel title element. | `String` | `'h1'` |
| contentSelector | The DOM selector for each panel content element. | `String` | `'div'` |
| wrapperCssClass | The CSS class added to container element. | `String` | `'accordion'` |
| panelCssClass | The CSS class added to each accordion panel element. | `String` | `'accordion__panel'` |
| triggerCssClass | The CSS class added to each accordion trigger element. | `String` | `'accordion__trigger'` |
| contentCssClass | The CSS class added to each accordion content element. | `String` | `'accordion__content'` |
| openClass | The CSS class added to a panel element when it is open. | `String` | `'is-open'` |


#### Exposed Methods
Methods that can be called once an instance of accordions is created.

| Method | Description | Arguments |
| ------ | ----------- | --------- |
| openPanel() | Opens a given panel at the given index. Ex: `accordionInst.openPanel(0)` | `headerIndex` type: `Number` |
| closePanel() | Closes a given panel at the given index. Ex: `accordionInst.closePanel(0)` | `headerIndex` type: `Number` |
| openAllPanels() | Opens all panels. |
| closeAllPanels() | Closes all panels. |
| destroy() | Removes all internal event listeners, CSS classes, and DOM elements added by the accordion plugin. |
| addEventListener() |  Adds a listener for the specified event. For a list of events, see the [Events](#events) section below. Ex: `accordionInst.addEventListener('panelOpen', onPanelOpenHandler)` | `eventType` type: `String` | `handler` type: `Function` |
| removeEventListener() |  Removes a listener for the specified event. For a list of events, see the [Events](#events) section below. Ex: `accordionInst.removeEventListener('panelOpen', onPanelOpenHandler)` | `eventType` type: `String` | `handler` type: `Function` |



## Events
The accordion fires several custom events, which can be subscribed to using the `addEventListener()` method. All event names are prefixed with `degui.accordion:`.

| Event Name | Description | Event Data |
| ---------- | ----------- | ---------- |
| degui.accordion:panelOpen | Fires after an accordion panel opens | `{ panelIndex: 0 }`
| degui.accordion:panelClose | Fires after an accordion panel closes | `{ panelIndex: 0 }`


## Accessibility
The accordion component follows the [WAI-ARIA example implementation](https://www.w3.org/TR/wai-aria-practices/examples/accordion/accordion.html) for accessible components.

### Keyboard Support
| Key | Function |
| --- | -------- |
| `Space` or `Enter` | When focus is on the accordion header of a collapsed section, expands the section. |
| `Down Arrow` | When focus is on an accordion header, moves focus to the next accordion header. If focus is on the last accordion header, moves focus to first accordion header. |
| `Up Arrow` | When focus is on an accordion header, moves focus to the previous accordion header. If focus is on first accordion header, moves focus to last accordion header. |
| `Home` | When focus is on an accordion header, moves focus to the first accordion header. |
| `End` | When focus is on an accordion header, moves focus to the last accordion header. |

## Polyfills required
- [Element.closest](https://developer.mozilla.org/en-US/docs/Web/API/Element/closest#Polyfill)
- [Array.findIndex](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex#Polyfill)
- [CustomEvent](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent#Polyfill)