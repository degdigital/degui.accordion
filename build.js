const fs = require('fs');
const Mustache = require('mustache');
const UglifyJS = require("uglify-es");

function handleError(err) {
    if (err) {
        throw err;
    }
}

fs.mkdir('dist', { recursive: true }, handleError);



// ---------------build markup--------------------


const view = JSON.parse(fs.readFileSync("src/accordion.json", "utf8"));
const template = fs.readFileSync("src/accordion.mustache", "utf8");
const output = Mustache.render(template, view);

fs.open('dist/accordion.html', 'w', function (err, file) {
    handleError(err);
    fs.writeFile('dist/accordion.html', output, handleError);
});



//------------ build css -----------------


fs.copyFile('src/accordion.css', 'dist/accordion.css', handleError)



//-------------- build js ------------------


fs.copyFile('src/accordion.js', 'dist/accordion.js', handleError);


const accordionJSFile = fs.readFileSync("src/accordion.js", "utf8");
const minaccordionJSFile = UglifyJS.minify(accordionJSFile).code;

fs.open('dist/accordion.min.js', 'w', function (err, file) {
    handleError(err);
    fs.writeFile('dist/accordion.min.js', minaccordionJSFile, handleError);
});





