const EVENT_PANEL_OPEN = 'degui.accordion:panelOpen';
const EVENT_PANEL_CLOSE = 'degui.accordion:panelClose';

const defaults = {
    allowMultiplePanelsOpen: false,
    initialOpenPanelIndices: [0],
    panelSelector: 'section',
    titleSelector: 'h1',
    contentSelector: 'div',
    wrapperCssClass: 'accordion',
    panelCssClass: 'accordion__panel',
    triggerCssClass: 'accordion__trigger',
    contentCssClass: 'accordion__content',
    openClass: 'is-open'
};

const accordion = function(element, options = {}) {    
    let settings;
    let panelEls;
    let contentWrapperEls = [];
    let triggerEls = [];

    function init() {
        settings = {...defaults, ...options};

        if(!element) {
            console.error('Accordion Error: no container element provided to accordion.')
        } else {
            element.classList.add(settings.wrapperCssClass);

            panelEls = [...element.querySelectorAll(settings.panelSelector)];
            panelEls.forEach(preparePanel);

            if(panelEls.length > 0) {
                openInitialPanels();
                bindEvents();
            }
        }
    }

    function bindEvents() {
        triggerEls.forEach(triggerEl => {
            triggerEl.addEventListener('click', onTriggerClick);
            triggerEl.addEventListener('keydown', onKeydown);
        });
    }

    function preparePanel(panelEl, index) {
        panelEl.classList.add(settings.panelCssClass);

        const titleEl = panelEl.querySelector(settings.titleSelector);
        const contentEl = panelEl.querySelector(settings.contentSelector);

        if(titleEl && contentEl) {
            const panelId = panelEl.id ? panelEl.id : `accordion-panel-${index}`;

            triggerEls.push(createTrigger(titleEl, panelId));
            contentWrapperEls.push(createContentWrapper(contentEl, panelEl, panelId));
        }
    }

    function createTrigger(titleEl, panelId) {
        const html = 
            `<button class="${settings.triggerCssClass}" 
                id="accordion-trigger--${panelId}" 
                aria-controls="accordion-content--${panelId}" 
                aria-expanded="false"
                type="button"
            ></button>`;

        titleEl.insertAdjacentHTML('afterbegin', html);
        
        const buttonEl = titleEl.firstElementChild;
        
        while(titleEl.childNodes.length > 1) {
            buttonEl.appendChild(titleEl.childNodes[1]);
        }

        return buttonEl;
    }

    function createContentWrapper(contentEl, panelEl, panelId) {
        const html = `
            <div class="${settings.contentCssClass}" 
                id="accordion-content--${panelId}" 
                aria-labelledby="accordion-trigger--${panelId}" 
                aria-hidden="true"
                role="region"
            ></div>`;

        panelEl.insertAdjacentHTML('beforeend', html);
        const contentWrapperEl = panelEl.lastElementChild;
        contentWrapperEl.appendChild(contentEl);
        return contentWrapperEl;
    }

    function onTriggerClick(e) {
        const buttonEl = e.target.closest(`.${settings.triggerCssClass}`);
        const targetIndex = triggerEls.indexOf(buttonEl);
        if (targetIndex > -1) {
            if (contentWrapperEls[targetIndex].classList.contains(settings.openClass)) {
                closePanel(targetIndex);
            } else {
                openPanel(targetIndex);
            }
        }
    }

    function onKeydown(e) {
        const {target, key} = e;
        
        switch(key) {
            case 'ArrowUp':
            case 'Up':
            case 'ArrowDown':
            case 'Down':
                e.preventDefault();
                focusDifferentTriggerEl(target, key);
                break;
            case 'Home':
                e.preventDefault();
                triggerEls[0].focus();
                break;
            case 'End':
                e.preventDefault();
                triggerEls[triggerEls.length - 1].focus();
                break;
        }
    }

    function focusDifferentTriggerEl(currentlyFocusedEl, key) {
        const currentIndex = triggerEls.indexOf(currentlyFocusedEl);
        const nextIndexDirection = key === 'ArrowDown' || key === 'Down' ? 1 : -1;
        const numTriggers = triggerEls.length;
        // handles wrap-around
        const nextIndex = (currentIndex + numTriggers + nextIndexDirection) % numTriggers;
        triggerEls[nextIndex].focus();
    }

    function getPanelIndexById(id) {
        return panelEls.findIndex(panelEl => panelEl.id === id);
    }

    function expandPanelByIndex(index) {
        expandPanel(triggerEls[index], contentWrapperEls[index]);
    }

    function openInitialPanels() {
        const hashVal = location.hash.replace('#', '');
        if(hashVal.length > 0) {
            const hashPanelIndex = getPanelIndexById(hashVal);
        
            if (hashPanelIndex > -1) {
                expandPanelByIndex(hashPanelIndex);
                return;
            } 
        }

        if(settings.initialOpenPanelIndices && settings.initialOpenPanelIndices.length) {
            const initialOpenPanelIndices = settings.allowMultiplePanelsOpen ?
                settings.initialOpenPanelIndices :
                [settings.initialOpenPanelIndices[0]];

            initialOpenPanelIndices.forEach(panelIndex => {
                expandPanelByIndex(panelIndex);
            });
        }
    }

    function expandPanel(triggerEl, contentWrapperEl) {
        triggerEl.setAttribute('aria-expanded', 'true');
        contentWrapperEl.setAttribute('aria-hidden', 'false');
        contentWrapperEl.classList.add(settings.openClass);
        
        fireEvent(EVENT_PANEL_OPEN, {
            panelIndex: triggerEls.indexOf(triggerEl)
        });
    }

    function collapsePanel(triggerEl, contentWrapperEl) {
        triggerEl.setAttribute('aria-expanded', 'false');
        contentWrapperEl.setAttribute('aria-hidden', 'true');
        contentWrapperEl.classList.remove(settings.openClass);

        fireEvent(EVENT_PANEL_CLOSE, {
            panelIndex: triggerEls.indexOf(triggerEl)
        });
    }

    function openPanel(panelIndex) {
        expandPanelByIndex(panelIndex);
        if(!settings.allowMultiplePanelsOpen) {
            contentWrapperEls.forEach((contentWrapperEl, index) => {
                if(index !== panelIndex) {
                    collapsePanel(triggerEls[index], contentWrapperEl);
                }
            })
        }
    }

    function closePanel(panelIndex) {
        collapsePanel(triggerEls[panelIndex], contentWrapperEls[panelIndex]);
    }

    function openAllPanels() {
        if(settings.allowMultiplePanelsOpen) {
            for(let i = 0; i < contentWrapperEls.length; i++) {
                openPanel(i);
            }
        } else if(contentWrapperEls.length) {
            openPanel(0);
        }
    }

    function closeAllPanels() {
        for(let i = 0; i < contentWrapperEls.length; i++) {
            closePanel(i);
        }
    }

    function destroyTrigger(triggerEl) {
        triggerEl.removeEventListener('click', onTriggerClick);
        triggerEl.removeEventListener('keydown', onKeydown);

        unwrapElement(triggerEl);
    }

    function unwrapElement(el) {
        const parentEl = el.parentNode;

        while (el.firstChild) {
            parentEl.insertBefore(el.firstChild, el);
        }

        parentEl.removeChild(el);

        el = null;
    }

    function destroy() {
        element.classList.remove(settings.wrapperCssClass);

        panelEls.forEach(panelEl => panelEl.classList.remove(settings.panelCssClass));

        while(triggerEls.length) {
            destroyTrigger(triggerEls.pop());
        }        

        while(contentWrapperEls.length) {
            unwrapElement(contentWrapperEls.pop());
        }
    }

    function addEventListener(type, listener, useCapture) {
        element.addEventListener(type, listener, useCapture);
    }

    function removeEventListener(type, listener, useCapture) {
        element.removeEventListener(type, listener, useCapture);
    }

    function fireEvent(eventType, data={}) {
        const event = new CustomEvent(eventType, { 
            bubbles: true,
            cancelable: false,
            detail: data
        });
        element.dispatchEvent(event);
    }

    init();

    return {
        openPanel,
        closePanel,
        openAllPanels,
        closeAllPanels,
        destroy,
        addEventListener,
        removeEventListener
    }
}

export default accordion;